import SignIn from './SignIn';

const Navbar = () => {
  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <h1>React Chat</h1>
      </div>
      <div className="navbar-actions">
        <SignIn /> 
      </div>
    </nav>
  ); 
}

export default Navbar;